# README - PS5 OXOCARD - Site

## Description
Ce dépôt contient le site réalisé pour le projet de semestre 5. Les différents dossiers de celui-ci contiennent :

- **docs/** : Contient les pages du site en Markdown, ainsi que leurs ressources associées.
- **overrides/** : Contient le fichier JavaScript, la feuille de style CSS et des ressources statiques supplémentaires pour personnaliser l'apparence du site.
- **README.md** : Le fichier que vous êtes en train de lire, fournissant des informations générales sur le projet.
- **pyproject.toml** et **poetry.lock** : Fichiers associés à la configuration du projet avec Poetry.
- **mkdocs.yml** : Fichier de configuration pour MkDocs, décrivant la structure de la documentation.
- **.gitlab-ci.yml** : Fichier de configuration CI/CD pour GitLab.
<br>
<br>
<br>
## Localhost

Il est possible de faire fonctionner le site en localhost. Pour ce faire, il est nécessaire de posséder les logiciels suivants : 
 - Python 3.11
 - Poetry 

Une fois ces logiciels installés, il ne reste plus qu'à utiliser les commandes suivantes : 

```
poetry install
poetry run mkdocs serve
```