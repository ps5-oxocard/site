# LED
<!------------------------------------------------------------------------------
- @brief       : Led exercices
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : LED.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->
## Câblage
### Introduction
Bienvenue à cet atelier dédié aux systèmes embarqués, où nous explorerons les bases de l'électronique en mettant en œuvre des composants simples pour créer des fonctionnalités lumineuses. Dans cet atelier, nous allons plonger dans le monde des microcontrôleurs et des LED, en mettant en pratique des concepts fondamentaux de l'embarqué.

Pour commencer, nous débuterons avec quelque chose de simple mais essentiel : allumer une LED rouge. Nous suivrons un schéma précis pour le câblage, puis nous mettrons en œuvre le code nécessaire pour activer la LED. Cela jettera les bases de notre exploration des systèmes embarqués et des manipulations électriques de base.

Une fois que nous aurons maîtrisé l'allumage de la LED rouge, nous passerons à une étape plus dynamique. Nous apprendrons à faire clignoter la LED en ajustant les délais et en contrôlant le cycle d'allumage et d'extinction. Cette partie introduira des concepts cruciaux tels que la gestion du temps dans la programmation embarquée.

Dans la troisième partie de notre atelier, nous élèverons la complexité en introduisant une LED tricolore (RGB). Vous apprendrez à câbler cette LED spéciale et à lui fournir des valeurs d'intensité pour produire une gamme de couleurs. Cela vous donnera un aperçu des possibilités créatives que les systèmes embarqués offrent en matière de contrôle des composants.

Pour conclure en beauté, nous ajouterons une dimension interactive à notre système embarqué en connectant un capteur de mouvement PIR (Passive Infrared). Nous programmerons le système pour que la LED tricolore change de couleur en fonction des détections du PIR. Cette partie vous donnera un avant-goût des applications pratiques des systèmes embarqués dans le domaine de la détection et de la réaction en temps réel.

L'atelier se fera en tout avec ces composants : 
![Composants nécessaires](page_TP/atelierLed/ressources/composants.png){: style="height:600px;width:600px"; align=center}<br>

### Les Besoins
 - Un ordinateur
 - Une connexion à Internet
 - Une Oxocard Connect
 - Un câble USB-USB type C

Il est également nécessaire d'avoir les composants électroniques suivants pour cette étape : 
![Composants nécessaires](page_TP/atelierLed/ressources/ledComp.png){: style="height:400px;width:300px"; align=center}<br>

Vous devrez alors avoir le montage suivant : <br><br>
![Vue des 3 côtés du circuit](page_TP/atelierLed/ressources/led3sides.png){: style="height:300px;width:700px"; align=center}<br>

Il est important de bien relier la LED à la pin 5 afin que cela concorde avec le code. Si vous voulez quand même utiliser une autre pin alors il faudra préciser dans la partie du code "Constantes and globales variables for IO" que LEDRED_PIN devra être égale à la C_PIN_0X où X est la pin désirée. 

Vous êtes dès à présent prêts à coder ! 
### Quelques explications

- Qu'est-ce qu'une LED ?

Une LED, c'est un peu comme une toute petite lumière spéciale que l'on peut allumer. Elle est utilisée dans beaucoup de choses autour de nous, comme les écrans de nos téléphones ou les voyants lumineux sur les appareils électroniques.

- Comment ça fonctionne ?

À l'intérieur de la LED, il y a deux parties spéciales qui s'appellent des "électrons" et des "trous". Quand on envoie de l'électricité à travers la LED, ces électron et trous se rencontrent au centre de la LED. Cela crée de l'énergie qui, à son tour, produit de la lumière. C'est comme si ces petits éléments dans la LED bougeaient très vite et c'est ce qui illumine la LED.

 - Pourquoi ça change de couleur ?

Ce qui est vraiment cool, c'est que différentes LEDs peuvent émettre différentes couleurs de lumière. Certaines sont rouges, d'autres vertes, et d'autres bleues. Tu peux même en trouver qui peuvent changer de couleur en fonction de ce que tu veux.

### Le code
Dans cette partie vous allez devoir trouver le code manquant à l'application afin d'allumer la LED : 

``` py
#####################################
# Constantes and globales           #
# variables for IO                  #
#####################################
const LEDRED_PIN = C_PIN_05        # LED on IO05

#####################################
# Constantes and globales           #
# variables for states and values   #
#####################################
const ON = 1                       # HIGH STATE / ON
const OFF = 0                      # LOW STATE / OFF

#####################################
# Init                              #
####-----------------------------####
initGPIO(..., ...)       # LED pin is an output
####-----------------------------####

#####################################
# Something to make the link blink  #
####-----------------------------####
writeGPIO(..., ...) 
####-----------------------------####
```

Pour vous aiguiller, essayer d'écrire le nom de la fonction dans l'onglet des informations à droite de l'éditeur de code. 
### Le Final

Le résultat est alors une LED qui peut s'allumer ou s'éteindre en fonction de votre valeur mise manuellement dans le code. 

![Resultat final](page_TP/atelierLed/ressources/ledfinal.png){: style="height:300px;width:600px"; align=center}<br>

C'est un bon début mais ne nous arrêtons pas ici ! Allez à l'étape deux afin d'essayer de la faire clignoter. 

## Clignoter

### Introduction
Vous avez maintenant une LED rouge brillante à votre disposition, mais nous ne nous arrêterons pas là. Dans cette deuxième partie, nous plongerons plus profondément dans le monde de la programmation embarquée en apprenant à donner vie à votre LED à travers des clignotements dynamiques.

Le simple fait d'allumer une LED est un excellent point de départ, mais pour tirer pleinement parti des capacités d'un microcontrôleur, nous devons apprendre à contrôler le temps. C'est ici que le concept de temporisation devient crucial. Dans cette session, nous explorerons comment ajuster les délais entre les états allumé et éteint de la LED pour créer un effet de clignotement fluide.

Nous aborderons des notions fondamentales telles que les boucles et la gestion du temps dans la programmation embarquée. Ces concepts sont essentiels pour créer des séquences lumineuses dynamiques et pour comprendre comment les systèmes embarqués interagissent avec le temps.

### Les Besoins

Pour cette partie, les mêmes éléments seront utilisés. Il n'y aura alors pas de câblage à faire.

### Quelques explications

Précisons ce qu'est une boucle "while" en programmation d'une manière simple.

Imagine que tu as une tâche à accomplir, mais tu veux la répéter plusieurs fois sans avoir à écrire la même chose encore et encore. La boucle "while" est un peu comme une règle qui dit à l'ordinateur de continuer à faire quelque chose tant qu'une condition particulière est vraie.

Comment ça fonctionne :

 - Condition : Avant de commencer la boucle, tu définis une condition. C'est comme une règle que tu donnes à l'ordinateur. Par exemple, tu pourrais dire "tant que le soleil brille".

 - Action : À l'intérieur de la boucle, tu mets toutes les actions que tu veux que l'ordinateur fasse. Par exemple, "jouer à l'extérieur".

 - Répétition : L'ordinateur va vérifier la condition (le soleil brille). Si la condition est vraie, il va faire les actions à l'intérieur de la boucle (jouer à l'extérieur). Ensuite, il reviendra vérifier la condition. Si elle est toujours vraie, il continuera à faire les actions encore et encore. Si la condition devient fausse, la boucle s'arrête. En programmation, une boucle "while" te permet de répéter une séquence d'actions tant qu'une certaine condition est vraie. C'est une façon efficace de faire faire à l'ordinateur des tâches répétitives sans avoir à réécrire le même code plusieurs fois.

### Le code

Dans cette partie vous allez devoir trouver le code manquant à l'application afin de faire clignoter la LED : 

``` py
#####################################
# Constantes and globales           #
# variables for IO                  #
#####################################
const LEDRED_PIN = C_PIN_05        # LED on IO05

#####################################
# Constantes and globales           #
# variables for states and values   #
#####################################
const ON = 1                       # HIGH STATE / ON
const OFF = 0                      # LOW STATE / OFF
const SLEEPTIME_FOR_BLINK = 100    # 100 .. 1000

#####################################
# Init                              #
#####################################
initGPIO(LEDRED_PIN, OUTPUT)       # LED pin is an output

# All colors OFF
writeGPIO(LEDRED_PIN, OFF) 


#####################################
# Main loop                         #
#####################################
while true:
    #####################################
	# Something to make the link blink  #
	####-----------------------------####
    # write the GPIO for the red LED on high state
    writeGPIO(...,...) 
    # make a delay, use the SLEEPTIME 
    delay(...)
    # write the GPIO for the red LED on low state
    writeGPIO(...,...) 
    #  make a delay, use the SLEEPTIME again
    delay(...)
    ####-----------------------------####
```

### Le Final

Le résultat est alors une LED qui clignote en fonction du temps que tu lui donne. 

![Resultat final](page_TP/atelierLed/ressources/blinkfinal.png){: style="height:300px;width:600px"; align=center}<br>

Essayons maintenant de changer de couleur avec la prochaine partie !


## Tricolor

### Introduction
Nous sommes maintenant prêts à franchir une nouvelle étape passionnante dans notre exploration des systèmes embarqués. Dans cette troisième partie, nous allons introduire la magie des couleurs en travaillant avec une LED tricolore (RGB) et en contrôlant les intensités lumineuses.

La LED tricolore offre une palette de possibilités lumineuses bien plus large, permettant de créer une variété infinie de couleurs en ajustant les niveaux d'intensité des trois couleurs de base : rouge, vert et bleu. Cela nous ouvre les portes à des expériences visuelles plus riches et créatives.

Au cours de cette session, vous apprendrez à câbler correctement une LED tricolore, puis à manipuler les valeurs d'intensité pour générer des couleurs spécifiques. Nous explorerons les bases de la gestion des couleurs en programmation embarquée et comment créer des variations lumineuses pour obtenir l'effet visuel désiré.

### Les Besoins
 - Un ordinateur
 - Une connexion à Internet
 - Une Oxocard Connect
 - Un câble USB-USB type C

Vous devrez alors avoir le montage suivant : <br><br>
![Vue des 3 côtés du circuit](page_TP/atelierLed/ressources/tricolor3sides.png){: style="height:300px;width:700px"; align=center}<br>

### Quelques explications

Imaginons que tu as des lumières spéciales, comme des ampoules, mais un peu plus intelligentes. Certaines de ces lumières ne savent faire qu'une seule couleur, comme le rouge. C'est ce qu'on appelle une LED rouge à deux pattes. Tu lui donnes de l'électricité, et elle s'allume en rouge, c'est simple !

Maintenant, parlons d'une lumière encore plus intéressante, une LED tricolore à trois pattes. Elle, elle sait faire du rouge, du vert et du bleu. Comment ça marche ? Chacune de ses pattes est comme un interrupteur pour une couleur différente. Si tu allumes le rouge, elle devient rouge. Si tu allumes le vert, elle devient verte. Et si tu allumes le bleu, elle devient bleue. Ce qui est très intéressant, c'est que tu peux ajuster l'intensité de chaque couleur. Intensité, c'est comme la force de la lumière.

Dans notre histoire, imagine que tu peux régler l'intensité de la lumière de 0 à 4096 (c'est les 3 constantes d'intensité dans notre code). Plus l'intensité est élevée, plus la couleur est forte. Et si tu joues avec ces intensités pour le rouge, le vert et le bleu, tu peux créer plein de couleurs différentes. Par exemple, en allumant le rouge et le bleu à fond, tu obtiens une lumière violette !

Alors voilà, une LED tricolore, c'est comme une lumière intelligente qui peut mélanger les couleurs en jouant avec l'intensité des trois couleurs de base. On reste alors sur le même principe qu'avant mais avec 3 couleurs. 

### Le code
``` py
#####################################
# Constantes and globales           #
# variables for IO                  #
#####################################
const LEDRGB_RED = C_PIN_02        # Red on IO02
const LEDRGB_GREEN = C_PIN_03      # Green on IO03
const LEDRGB_BLUE = C_PIN_04       # Blue on IO04
const LEDRED_PIN = C_PIN_05        # LED on IO05

#####################################
# Constantes and globales           #
# variables for states and values   #
#####################################
const ON = 1                       # HIGH STATE / ON
const OFF = 0                      # LOW STATE / OFF
const SLEEPTIME_FOR_BLINK = 100              # 100 .. 1000
const INTENSITY_RED = 4096            # 0 .. 4096
const INTENSITY_GREEN = 1371       # 0 .. 4096
const INTENSITY_BLUE = 0           # 0 .. 4096

#####################################
# Init                              #
#####################################
initGPIO(LEDRED_PIN, OUTPUT)       # LED pin is an output

# All colors OFF
writeGPIO(LEDRGB_RED, OFF) 
writePWM(LEDRGB_RED, 0)
writePWM(LEDRGB_GREEN, 0)
writePWM(LEDRGB_BLUE, 0)

#####################################
# Function to light led's blue part #
#####################################
def lightRed():
    #####################################
	# Something to insert here to use   #
	# 	change the intensity of red     #
	####-----------------------------####
    writePWM(LEDRGB_RED, ...)
    ####-----------------------------####

#####################################
# Function to light led's green part#
#####################################
def lightGreen():
    #####################################
	# Something to insert here to use   #
	# 	change the intensity of green   #
	####-----------------------------####
    writePWM(LEDRGB_GREEN, ...)
    ####-----------------------------####

#####################################
# Function to light led's red part  #
#####################################
def lightBlue():
    #####################################
	# Something to insert here to use   #
	# 	change the intensity of blue    #
	####-----------------------------####
    writePWM(LEDRGB_BLUE, ...)
    ####-----------------------------####

#####################################
# Main loop                         #
#####################################
while true:
    #####################################
	# Something to insert here to use   #
	# 	the tricolor led                #
	####-----------------------------####
   
    ####-----------------------------####
    writeGPIO(LEDRED_PIN, ON) 
    delay(SLEEPTIME_FOR_BLINK)
    writeGPIO(LEDRED_PIN, OFF) 
    delay(SLEEPTIME_FOR_BLINK)
```

### Le Final

Le résultat est alors une LED qui change de couleur en fonction de l'intensité qu'on lui donne. 

![Resultat final](page_TP/atelierLed/ressources/tricolorfinal.png){: style="height:300px;width:700px"; align=center}<br>

Essayons maintenant d'ajouter une touche intéractive avec la prochaine partie !


## Infrarouge

### Introduction
 Nous arrivons maintenant à la quatrième et dernière partie de notre atelier captivant sur les systèmes embarqués. Préparez-vous à ajouter une dimension interactive à votre projet en intégrant un Détecteur de Mouvement PIR (Passive Infrared).

Dans cette section, nous allons plonger dans le domaine de la détection de mouvement en temps réel. Le PIR est un capteur sensible qui réagit aux changements de température, tels que ceux produits par les mouvements humains. Nous utiliserons cette technologie pour créer une interaction dynamique entre votre LED tricolore et l'environnement.

Au cours de cette session, vous apprendrez à câbler le PIR de manière appropriée, à comprendre ses signaux de sortie et à les utiliser pour déclencher des actions spécifiques dans votre programme embarqué. Imaginez la LED tricolore changeant de couleur chaque fois qu'un mouvement est détecté, offrant ainsi des applications pratiques pour la signalisation lumineuse intelligente.

Cette partie de l'atelier vous fournira un aperçu concret de la manière dont les systèmes embarqués peuvent réagir en temps réel à des stimuli extérieurs, ouvrant ainsi la voie à des applications telles que la sécurité, la surveillance et bien d'autres.

### Les Besoins
 - Un ordinateur
 - Une connexion à Internet
 - Une Oxocard Connect
 - Un câble USB-USB type C

Vous devrez alors avoir le montage suivant : <br><br>
![Vue des 3 côtés du circuit](page_TP/atelierLed/ressources/infrared3sides.png){: style="height:300px;width:700px"; align=center}<br>
### Quelques explications
Imagine qu'il y ait une petite boule blanche dans un coin de la pièce. Cette boule n'est pas ordinaire, elle est comme un détective qui peut sentir la chaleur des objets autour d'elle. Les objets, comme les gens, émettent de la chaleur sous forme de rayons infrarouges (que nous ne pouvons pas voir).

![PIR représentation](page_TP/atelierLed/ressources/PIR.png){: style="height:400px;width:450px"; align=center}<br>

Le détecteur à infrarouge PIR est comme ce détective. Quand quelqu'un entre dans la pièce et émet de la chaleur, le détecteur PIR le remarque. Il fonctionne un peu comme un interrupteur qui dit à la lumière : "Hé, quelqu'un est là ! Allume-toi !" C'est une façon pratique de faire en sorte que la lumière s'allume automatiquement quand quelqu'un entre dans la pièce, sans avoir besoin de toucher à un interrupteur.
### Le code
``` py
#####################################
# Constantes and globales           #
# variables for IO                  #
#####################################
const LEDRGB_RED = C_PIN_02        # Red on IO02
const LEDRGB_GREEN = C_PIN_03      # Green on IO03
const LEDRGB_BLUE = C_PIN_04       # Blue on IO04
const LEDRED_PIN = C_PIN_05        # LED on IO05
const PIR_PIN = C_PIN_06           # PIR sensor on IN06
#####################################
# Constantes and globales           #
# variables for states and values   #
#####################################
const ON = 1                       # HIGH STATE / ON
const OFF = 0                      # LOW STATE / OFF
count = 0
const THREESECONDES = 30
const SLEEPTIME = 100
#####################################
# Init                              #
#####################################
initGPIO(PIR_PIN, INPUT)           # PIR is an input
initGPIO(LEDRED_PIN, OUTPUT)       # LED pin is an output

# All colors OFF
writeGPIO(LEDRGB_RED, OFF) 
writePWM(LEDRGB_RED, 0)
writePWM(LEDRGB_GREEN, 0)
writePWM(LEDRGB_BLUE, 0)
#####################################
# Function to light LED in yellow   #
#####################################
def lightInYellow():
    writePWM(LEDRGB_RED, 3072)
    writePWM(LEDRGB_GREEN, 512)
    writePWM(LEDRGB_BLUE, 0)

#####################################
# Function to light LED in green    #
#####################################
def lightInGreen():
    writePWM(LEDRGB_RED, 0)
    writePWM(LEDRGB_GREEN, 4096)
    writePWM(LEDRGB_BLUE, 0)

#####################################
# Function to light LED in red      #
#####################################
def lightInRed():
    writePWM(LEDRGB_RED, 4096)
    writePWM(LEDRGB_GREEN, 0)
    writePWM(LEDRGB_BLUE, 0)

#####################################
# Main loop                         #
#####################################
while true:
    # If infrared detected
	#####################################
	# Something to insert here to read  #
	# 	the PIR                     #
	####-----------------------------####
	if readGPIO(...):  
    ####-----------------------------####
        count++
        # If infrared detected for less than 2 secondes
        if(count < THREESECONDES):
            lightInYellow()
        # If infrared detected for 2 secondes or more
        else:
            lightInRed()
    else: 
        lightInGreen()
        count = 0
    delay(SLEEPTIME)
     
```
### Le Final
Le résultat est alors une LED qui s'allume et change de couleur lorsque le détecteur capte un mouvement. 

![Resultat final](page_TP/atelierLed/ressources/infraredfinal.png){: style="height:300px;width:700px"; align=center}<br>

Bravo tu es arrivé à la fin de cet atelier et tu en sais désormais plus sur la programmation de systèmes embarqués. Si tu désires continuer avec cet environnement de développement, tu peux te procurer ton propre kit OXOCARD Connect ici : <https://shop.oxon.ch/?shop=oxocard-connect&lang=fr>
