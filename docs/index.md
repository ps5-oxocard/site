# Oxocard - A way to make computer science attractive to a young audience
<!------------------------------------------------------------------------------
- @brief       : Index page
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : index.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->

![MCU](page_Accueil/ressources/cardsOxocard.png){: style="height:250px;width:600px"}

Bienvenue aux ateliers oxocard

## Url vers le site
    https://ps5-oxocard.pages.forge.hefr.ch/site

