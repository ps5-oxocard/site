# Température
<!------------------------------------------------------------------------------
- @brief       : Temperature exercices
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : temp.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->
## Introduction

Bienvenue dans le chapitre "Thermomètre" ! Ici, nous allons explorer comment créer un thermomètre virtuel qui nous permettra de mesurer et d'afficher la température. Ce code est écrit en utilisant le langage de programmation Nanopy, qui nous aidera à construire un thermomètre visuel interactif.<br>
![Thermo fonctionnel_2](page_FETG/ressources/Temperature_2.png){: style="height:250px;width:250px"; align=center}<br>
## Les Besoins
 - Un ordinateur
 - Une connexion à Internet
 - Une Oxocard Science
 - Un câble USB-USB type C
## Le code
Le code commence par définir des "constantes". Il s'agit de valeurs qui restent fixes tout au long de l'exécution du programme. Par exemple, `X` est défini à 47, ce qui signifie que c'est la position de départ du thermomètre. `WIDTH` est fixé à 28, déterminant la largeur du thermomètre, et `COLOR` est égal à 0, ce qui est lié aux couleurs.
``` py
const X = 47       # 0 .. 200
const WIDTH = 28   # 10 .. 80
const COLOR = 0    # HUE
```
`Y1` et `Y2` déterminent où commence et finit le thermomètre sur l'écran. Pensez à `Y1` comme au point de départ du thermomètre en haut de l'écran, et à `Y2` comme au point de fin en bas de l'écran. Ces valeurs contrôlent à quel endroit le thermomètre apparaît sur l'écran.
``` py
const Y1 = 20      
const Y2 = 180   
```
`const min_value` et `const max_value` définissent les températures minimale et maximale que le thermomètre peut mesurer. La température la plus basse qu'il peut afficher est de -20 degrés Celsius, et la plus haute est de 44 degrés Celsius. Ces nombres nous disent dans quelle gamme de température le thermomètre fonctionne.
``` py
const min_value = -20 # °C
const max_value = 44 # °C
```
`R` et `R2 = R*1.5` servent calculer la taille des cercles dans le thermomètre. R est la moitié de la largeur du thermomètre, ce qui nous donne un cercle au milieu. R2 est un peu plus grand, 1,5 fois la taille de R, créant un cercle sous le premier. Ces cercles aident à dessiner le thermomètre.
``` py
R:float = WIDTH/2
R2:float = R*1.5
```
La méthode `drawEmptyThermometer()` est un peu comme un ensemble d'instructions magiques qui nous permettent de dessiner un thermomètre vide sur l'écran. Imaginez que vous ayez un crayon virtuel et que vous suivez ces instructions pour créer un thermomètre. Tout commence par `push()`, qui est comme une "photo" de ce que nous avons dessiné jusqu'à présent, afin de pouvoir y revenir plus tard.
``` py
def drawEmptyThermometer():
    push()
    stroke(255,255,255)
    fill(0,0,0)
    drawLine(X, Y1, X, Y2)
    drawLine(X+WIDTH, Y1, X+WIDTH, Y2)
    drawCircle(X+R, Y1, R)
    fillHSV(COLOR,255,255)
    drawCircle(X+R, Y2+20, R2)
    noStroke()
    fill(0,0,0)
    drawRectangle(X+1, Y1+1, X+WIDTH-X-2, Y2-Y1-2)
    pop()
```
Ensuite, nous choisissons la couleur blanche pour le contour du thermomètre avec `stroke(255, 255, 255)`, et nous remplissons l'intérieur en noir en utilisant `fill(0, 0, 0)`. Ces couleurs définissent l'apparence de notre thermomètre.
``` py
stroke(255,255,255)
fill(0,0,0)
```
Les deux lignes verticales que nous dessinons avec `drawLine()` créent les bords du thermomètre. Une ligne part du haut du thermomètre (X, Y1) et va jusqu'au bas (X, Y2), et une autre fait la même chose de l'autre côté. C'est comme si vous dessiniez les côtés du tube du thermomètre.
``` py
drawLine(X, Y1, X, Y2)
drawLine(X+WIDTH, Y1, X+WIDTH, Y2)
```
Ensuite, nous ajoutons une petite "bosse" en haut du thermomètre en utilisant `drawCircle(X+R, Y1, R)`. Cette bosse lui donne une apparence plus réaliste. La couleur de cette bosse dépend de la valeur que nous avons choisie plus tôt.
``` py
drawCircle(X+R, Y1, R)
```
En bas du thermomètre, nous dessinons une base avec un autre cercle légèrement plus grand à l'aide de `drawCircle(X+R, Y2+20, R2)`. Cela donne au thermomètre une base solide.
``` py
fillHSV(COLOR,255,255)
drawCircle(X+R, Y2+20, R2)
```
Pour le reste du thermomètre, nous le remplissons entièrement en noir. Enfin, un rectangle recouvre la partie entre les lignes verticales, créant ainsi un tube creux qui ressemble à un thermomètre.
``` py
noStroke()
fill(0,0,0)
drawRectangle(X+1, Y1+1, X+WIDTH-X-2, Y2-Y1-2)
pop()
```
La méthode `drawTempValue()` est une partie importante de notre thermomètre virtuel. Elle nous permet d'afficher la valeur de la température à un endroit spécifique du thermomètre. Imaginons que nous ayons un stylo virtuel et que nous utilisions cette méthode pour écrire des chiffres sur notre thermomètre.
``` py
def drawTempValue(text:byte[], y:int):
    push()
    textFont(FONT_ROBOTO_16)
    drawText(X+WIDTH+10, y-10, text)
    pop()
```
nous utilisons `drawText(X+WIDTH+10, y-10, text)` pour écrire les chiffres de la température à un endroit précis. L'endroit est déterminé par `X+WIDTH+10` pour l'horizontale et `y-10` pour la verticale. Ces chiffres sont stockés dans text et sont affichés sur le thermomètre.
``` py
drawText(X+WIDTH+10, y-10, text)
```
Nous utilisons la fonction `drawLine()` pour dessiner des lignes horizontales sur notre thermomètre. Chaque ligne commence à un point précis sur le côté droit du thermomètre, à `X+WIDTH-10`, et va tout droit jusqu'à `X+WIDTH`. Ces lignes servent de repères pour différentes températures. Par exemple, la première ligne est dessinée à une hauteur de 30 pixels du haut du thermomètre, et elle représente une température de 40°C. Ensuite, la ligne suivante est tracée à 55 pixels du haut, indiquant 30°C, et ainsi de suite pour 20°C, 10°C, 0°C et -10°C.
``` py
def drawLines():
    drawLine(X+WIDTH-10, 30, X+WIDTH, 30)   # 40°C
    drawLine(X+WIDTH-10, 55, X+WIDTH, 55)   # 30°C
    drawLine(X+WIDTH-10, 80, X+WIDTH, 80)   # 20°C
    drawLine(X+WIDTH-10, 105, X+WIDTH, 105) # 10°C
    drawLine(X+WIDTH-10, 130, X+WIDTH, 130) # 0°C
    drawLine(X+WIDTH-10, 155, X+WIDTH, 155) # -10°C
```
Cette partie du code prépare le terrain pour que notre thermomètre puisse fonctionner. Imaginez que c'est comme le moment où vous préparez tous les outils avant de commencer un projet.
Nous commençons par appeler la méthode `drawEmptyThermometer()`. Cela crée notre thermomètre vide, prêt à afficher la température. 
``` py
drawLine(X+WIDTH-10, 30, X+WIDTH, 30)   # 40°C
drawLine(X+WIDTH-10, 55, X+WIDTH, 55)   # 30°C
drawLine(X+WIDTH-10, 80, X+WIDTH, 80)   # 20°C
drawLine(X+WIDTH-10, 105, X+WIDTH, 105) # 10°C
drawLine(X+WIDTH-10, 130, X+WIDTH, 130) # 0°C
drawLine(X+WIDTH-10, 155, X+WIDTH, 155) # -10°C
```
Ensuite, nous utilisons la méthode `drawTempValue()` pour écrire des nombres sur le thermomètre. Ces nombres représentent différentes températures. Par exemple, "40°C" est écrit à une hauteur de 30 pixels, ce qui signifie que lorsque le thermomètre affiche 40 degrés Celsius, c'est là que nous le verrons.
Nous ajoutons également d'autres valeurs de température à différentes hauteurs sur le thermomètre, comme "30°C", "20°C", "10°C", "0°C", et "-10°C". Cela crée des repères visuels pour nous aider à comprendre la température affichée par le thermomètre.
``` py
# Setup
drawEmptyThermometer()
drawTempValue("40°C", 30)
drawTempValue("30°C", 55)
drawTempValue("20°C", 80)
drawTempValue("10°C", 105)
drawTempValue("  0°C", 130)
drawTempValue("-10°C", 155)
```
Le premier morceau de code, entre les lignes `# SOMETHING TO INSERT HERE`, est un espace réservé pour vous. C'est là que vous pouvez ajouter vos propres instructions pour obtenir la température. Vous pourriez utiliser un capteur ou un dispositif pour cela. Une fois que vous avez mesuré la température, vous la stockez dans la variable value.
``` py
def fillThermometer()
    ##################################
    #    SOMETHING TO INSERT HERE
    #     TO GET THE TEMPERATURE
    ##################################
   

    value = ...


    ##################################

    noStroke()
    fillHSV(COLOR,255,255)
    currentHeight = map(value, min_value, max_value, Y2, Y1)
    drawRectangle(X+1, currentHeight, X+WIDTH-X-2, Y2-currentHeight+10)
    
```
Ensuite, nous utilisons `noStroke()` pour dire au programme de ne pas dessiner de contours. Cela rendra notre thermomètre encore plus propre et simple.Avec `fillHSV(COLOR, 255, 255)`, nous choisissons la couleur pour remplir notre thermomètre. La couleur dépend de la valeur de COLOR que nous avons définie précédemment. Ensuite, nous calculons où afficher la température sur le thermomètre. Cela se fait en utilisant `map(value, min_value, max_value, Y2, Y1)`. Cela signifie que la hauteur de l'affichage de la température dépend de la valeur mesurée, et cela nous aide à montrer la température correcte sur le thermomètre. Finalement, nous utilisons `drawRectangle()` pour remplir une partie du thermomètre en fonction de la température mesurée. C'est comme si nous remplissions le thermomètre avec de la couleur pour montrer la température.
``` py
noStroke()
fillHSV(COLOR,255,255)
currentHeight = map(value, min_value, max_value, Y2, Y1)
drawRectangle(X+1, currentHeight, X+WIDTH-X-2, Y2-currentHeight+10) 
```
Pour finir, la fonction `onDraw()` est appelé en boucle par l'appareil et redessine l'écran en continue avec les fonctions qu'on lui a dit de faire en boucle. 
``` py
def onDraw():
    drawEmptyThermometer()
    fillThermometer()
    drawLines()
    update()
    if getButton():
        if returnToMenu():
            return
```
## Le Final
Le résultat est un thermomètre qui se rempli en fonction de la température présente dans la salle

![Thermo fonctionnel](page_FETG/ressources/Temperature.png){: style="height:300px;width:200px"; }