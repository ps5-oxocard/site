# Bienvenue
<!------------------------------------------------------------------------------
- @brief       : Welcome page of "Futur En Tous Genres"
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : futurEnTousGenres.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->
Bienvenue à notre passionnant atelier de programmation ! Nous sommes ravis de vous accueillir aujourd'hui. <br>

La programmation est bien plus qu'une simple compétence technique. C'est un outil puissant qui vous permettra de créer des applications, des sites web et bien plus encore. Vous découvrirez comment les logiciels et les machines interagissent pour créer le monde numérique qui nous entoure. <br>

Au cours de cet atelier, vous apprendrez les bases de la programmation et développerez des applications sur un petit système embarqué. Peu importe que vous soyez débutant ou que vous ayez déjà un peu d'expérience en programmation, cet atelier est conçu pour vous aider à progresser et à vous amuser en même temps. <br>

N'oubliez pas que la programmation est une aventure qui permet de développer votre créativité, votre logique et votre pensée critique. C'est un excellent moyen de préparer votre avenir, car les compétences en programmation sont de plus en plus recherchées dans de nombreux domaines.

# Quelques préparatifs...
Quelques petites étapes sont à faire avant de pouvoir commencer. 

1. Aller sur l'éditeur 

    - [L'éditeur NanopPy](https://editor.nanopy.io/ )
    

2. Avoir une carte Oxocard Science à sa place

    