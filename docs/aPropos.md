# À Propos
<!------------------------------------------------------------------------------
- @brief       : Presentation of the creator
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : aPropos.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->
## Kais Ouederni

### Localisation

 - Originaire de Châtel-St-Denis, Suisse
 - Habite actuellement à Bulle

### Parcours

 - Formation en électronique chez Bobst SA
 - Voyage linguistique à San Diego pour améliorer l'anglais
 - Formation en informatique à l'École de Métiers de Fribourg
 - Service militaire dans le domaine médical
 - Voyage linguistique au Japon pour apprendre le japonais
 - Actuellement en bachelor à la Haute École d'Ingénieur de Fribourg

### Intérêts

 - Passionné de jeux vidéo
 - Intérêt marqué pour les technologies de télécommunication et les systèmes embarqués
