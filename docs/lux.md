# Lumière
<!------------------------------------------------------------------------------
- @brief       : Lux exercices
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : lux.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->
## Introduction
Bienvenue à l'atelier de création de lampes interactives ! Dans cet atelier, nous allons explorer comment dessiner une lampe sur un écran, et ce qui la rend spéciale, c'est qu'elle s'illumine en fonction de la lumière ambiante. Vous allez découvrir comment utiliser la technologie pour créer quelque chose d'amusant et interactif.

Imaginez que vous puissiez créer votre propre lampe virtuelle et qu'elle puisse briller plus fort ou plus doucement en fonction de la lumière réelle qui vous entoure. 

Dans cet atelier, nous allons explorer comment cela fonctionne. Vous allez voir que vous pouvez personnaliser votre lampe, lui donner une couleur spéciale et la faire réagir à la lumière dans votre environnement.<br>
![Lamp fonctionnel_2](page_FETG/ressources/Lamp_2.png){: style="height:250px;width:250px"; }<br>
Cet atelier se réalise à deux, une personne va réaliser la lampe. L'autre personne va réaliser lampe torche avec son écran et pourra ajuster la luminosité de celle-ci afin d'allumer la lampe de son binôme. 

## Les Besoins
 - Un ordinateur
 - Une connexion à Internet
 - Une Oxocard Science
 - Un câble USB-USB type C
## Lampe : Le code
Dans notre code, nous utilisons deux "règles spéciales", appelées constantes, pour définir la couleur de notre lampe et à quel point elle brillera.<br>

 - La première règle s'appelle `hue` (teinte). C'est comme la couleur de base de notre lampe. Imaginez que vous puissiez choisir parmi différentes couleurs, comme le rouge, le bleu, le vert, etc. La valeur 46 que nous avons choisie signifie que notre lampe aura une couleur spécifique, un peu comme si vous choisissiez votre couleur préférée pour peindre quelque chose.<br>

 - La deuxième règle s'appelle `saturation`. C'est comme le fait de décider à quel point la couleur de notre lampe sera éclatante. Avec une valeur de 255, cela signifie que la couleur de notre lampe sera très vive et lumineuse. Si nous choisissions une valeur plus basse, la couleur serait plus douce et moins vive.<br>

Vous pouvez les ajuster avec le slider sur la droite. <br>
``` py
const hue = 46          # HUE
const saturation = 255  # 0 .. 255
```
Dans cette partie du code, nous avons d'autres constantes qui nous aident à contrôler la luminosité de notre lampe en fonction de la lumière ambiante.<br>

 - `const LUX_Minimum = 100` : Cette règle nous indique la quantité minimale de lumière nécessaire pour que notre lampe s'éclaire. Imaginez que votre lampe s'allume uniquement lorsque la pièce est suffisamment éclairée. 100 représente cette quantité minimale. Si la lumière descend en dessous de ce niveau, la lampe s'éteint.<br>

 - `const LUX_Maximum = 10000` : C'est le contraire de la règle précédente. Elle indique le niveau maximum de lumière que notre lampe peut détecter. En d'autres termes, si la lumière devient vraiment très forte, la lampe atteindra sa luminosité maximale à 10 000. Au-delà de ce niveau, la lampe ne brille pas plus fort.<br>

 - `luxValue:float` : Ici, luxValue est un endroit où nous gardons en mémoire la quantité de lumière actuelle. C'est comme avoir une "boîte" pour stocker le niveau de lumière à un moment donné.<br>

 - `rateMax = 255.0` : rateMax est la luminosité maximale que notre lampe peut atteindre. La valeur 255 signifie que la lampe peut briller très fort, presque comme un projecteur. C'est la luminosité maximale que notre lampe peut avoir.<br>

 - `rate = 0.0` : Au départ, la luminosité de notre lampe est réglée à 0.0. Cela signifie que la lampe est éteinte. Au fur et à mesure que la lumière ambiante augmente, rate augmentera également, et la lampe s'allumera progressivement.<br>
``` py
const LUX_Minimum = 100
const LUX_Maximum = 10000
luxValue:float
rateMax = 255.0
rate = 0.0
lux3 = 3
```
Dans cette partie du code, nous avons une "fonction" spéciale appelée `drawLamp()`. Cette fonction est comme une série d'instructions pour dessiner une lampe sur notre écran. <br>
``` py
def drawLamp():
    stroke(255, 255, 255)
    drawQuadrangle(160, 20, 
                200, 20, 
                220, 100,
                140, 100)
    fill(255, 255, 255)
    drawRectangle(175, 100, 10, 80)
    drawRectangle(150, 180, 60, 15)
    drawLine(200, 100, 200, 140)
    drawEllipse(200, 145, 5, 8)
    drawLine(160, 195, 160, 210)
    drawLine(160, 210, 5, 210)
    drawCircle(0, 210, 5)
    noFill()
```
Voici comment ça fonctionne :<br>
Tout d'abord, nous utilisons `stroke(255, 255, 255)` pour définir la couleur des traits que nous allons dessiner. Dans ce cas, les traits seront de couleur blanche, ce qui signifie qu'ils seront très visibles sur l'écran.<br>
``` py
def drawLamp():
    stroke(255, 255, 255)
    
```
Ensuite, nous utilisons différentes instructions pour dessiner les formes qui composent notre lampe. Par exemple, avec `drawQuadrangle()`, nous dessinons la forme principale de la lampe. C'est un rectangle avec deux côtés en biais pour donner l'apparence d'une lampe. Les nombres spécifiés, comme (160, 20) et (200, 20), indiquent les points de départ et d'arrêt pour le dessin de la forme.<br>
``` py
drawQuadrangle(160, 20, 
            200, 20, 
            220, 100,
            140, 100)
```
Nous utilisons `fill(255, 255, 255)` pour remplir certaines parties de la lampe en blanc. Par exemple, `drawRectangle(175, 100, 10, 80)` dessine le corps principal de la lampe, et `drawRectangle(150, 180, 60, 15)` dessine la base de la lampe.<br>
``` py
fill(255, 255, 255)
drawRectangle(175, 100, 10, 80)
drawRectangle(150, 180, 60, 15)
```
Les autres instructions, comme `drawLine()`, `drawEllipse()`, `drawCircle()`, et `drawQuadrangle()`, dessinent des parties spécifiques de la lampe, comme le câble, la prise et le corps.<br>
``` py
drawLine(200, 100, 200, 140)
drawEllipse(200, 145, 5, 8)
drawLine(160, 195, 160, 210)
drawLine(160, 210, 5, 210)
drawCircle(0, 210, 5)
``` 
Enfin, avec `noFill()`, nous disons au programme de ne pas remplir certaines parties de la lampe, ce qui les rend transparentes.<br>
``` py
noFill()
```
Dans cette partie du code, nous avons une "fonction" spéciale appelée `makeMoreOrLessLight()`. Cette fonction est responsable de faire briller notre lampe plus ou moins fort en fonction de la quantité de lumière ambiante autour d'elle. <br>
``` py
def makeMoreOrLessLight():
    ##################################
    #    SOMETHING TO INSERT HERE    #
    #   TO GET THE LUX IN THE ROOM   #
    ##################################

    luxValue = ...

    ##################################

    rate = ((luxValue*5) - LUX_Minimum) * (rateMax / (LUX_Maximum - LUX_Minimum))
    lux = strSubstring(((luxValue)+""), 0, 6)
    delay(500)
    if rate > 255:
        rate = 255
    elif rate < 0:
        rate = 0
    rateBackground = rate - 20
    if rateBackground < 0:
        rateBackground = 0
    
    x=175
    # Light the room from the lamp to the left
    while x > 0:
        if rateBackground < 0:
            rateBackground = 0
        fillHSV(hue, saturation, rateBackground)
        noStroke()
        drawRectangle(x,0,10,195)
        rateBackground = rateBackground - lux3
        x = x - lux3
    rateBackground = rate - 20
    x=175
    # Light the room from the lamp to the right
    while x < 240:
        if rateBackground < 0:
            rateBackground = 0
        fillHSV(hue, saturation, rateBackground)
        noStroke()
        drawRectangle(x,0,10,195)
        rateBackground = rateBackground - lux3
        x = x + lux3
    # Light the lamp
    fillHSV(hue, saturation, rate)
    drawQuadrangle(160, 20, 
                200, 20, 
                220, 100,
                140, 100)
    noFill()
```
Tout d'abord, entre les lignes `# SOMETHING TO INSERT HERE`, c'est l'endroit où vous pouvez insérer votre propre code pour mesurer la quantité de lumière ambiante. C'est comme si vous aviez un capteur de lumière pour dire à la lampe combien de lumière il y a autour d'elle. Plus il y a de lumière, plus la lampe brillera.<br>
``` py
    ##################################
    #    SOMETHING TO INSERT HERE    #
    #   TO GET THE LUX IN THE ROOM   #
    ##################################
    luxValue = ...
    ##################################
```
En utilisant la valeur `luxValue`, nous calculons à quelle intensité la lampe doit briller. Si la lumière est faible, la lampe brillera doucement, et si la lumière est forte, la lampe brillera plus fort. Cela se fait avec la formule :
``` py
rate = ((luxValue*5) - LUX_Minimum) * (rateMax / (LUX_Maximum - LUX_Minimum))

```
Nous utilisons `delay(500)` pour faire en sorte que les changements de luminosité ne se produisent pas trop rapidement. 
``` py
delay(500)
```
Ensuite, nous utilisons des conditions pour nous assurer que la luminosité de la lampe ne dépasse pas les limites. Si rate devient supérieur à 255 (le maximum), nous le réglons à 255 pour éviter qu'il ne devienne trop lumineux. De même, si rate devient inférieur à 0 (le minimum), nous le réglons à 0 pour éviter qu'il ne s'éteigne complètement.
``` py
if rate > 255:
    rate = 255
elif rate < 0:
    rate = 0
rateBackground = rate - 20
```
Enfin, nous utilisons des boucles `while` pour allumer des parties de la pièce en fonction de la luminosité de la lampe. C'est comme si la lampe projetait de la lumière sur les murs pour éclairer la pièce. Cela donne l'illusion que la lampe éclaire vraiment l'environnement.
``` py
# Light the room from the lamp to the left
while x > 0:
    if rateBackground < 0:
        rateBackground = 0
    fillHSV(hue, saturation, rateBackground)
    noStroke()
    drawRectangle(x,0,10,195)
    rateBackground = rateBackground - lux3
    x = x - lux3
rateBackground = rate - 20
x=175
# Light the room from the lamp to the right
while x < 240:
    if rateBackground < 0:
        rateBackground = 0
    fillHSV(hue, saturation, rateBackground)
    noStroke()
    drawRectangle(x,0,10,195)
    rateBackground = rateBackground - lux3
    x = x + lux3
```

Pour finir nous avons juste a dessiner périodiquement la fonction. 
``` py 
def onDraw():
    makeMoreOrLessLight()
    drawLamp()
	update()
	if getButton():
		if returnToMenu():
			return
```
## Lampe Torche : Code 
Dans ce code, nous avons trois règles spéciales pour définir l'apparence et la couleur de notre lampe torche.

 - `const hue = 90` : Cette règle nous permet de choisir la teinte de la couleur de notre lampe torche. La teinte est comme le "type" de couleur que nous voulons. Ici, une valeur de 90 signifie que notre lampe torche sera de couleur spécifique, un peu comme choisir entre le rouge, le bleu, le vert, etc.

 - `const saturation = 255` : Cette règle détermine à quel point la couleur de notre lampe torche sera vive. Une saturation de 255 signifie que la couleur sera très intense et éclatante, presque comme un flash de lumière. Si nous choisissions une valeur plus basse, la couleur serait moins vive et plus douce.

 - `const luminosity = 255` : Cela contrôle l'intensité lumineuse de notre lampe torche. Une valeur de 255 signifie que la lampe torche sera très brillante, presque comme un projecteur. Plus la luminosité est élevée, plus la lampe torche émettra de lumière. Si la luminosité était plus basse, la lumière serait moins intense.

Ces trois règles spéciales peuvent être changées à droite avec le slider. 
``` py
const hue = 90          # HUE
const saturation = 255  # 0 .. 255
const luminosity = 255  # 0 .. 255
```
Ensuite nous avons quatre valeurs spéciales qui définissent la taille et la position de l'écran de notre lampe torche.

 - `xMinScreen = 0` : Cela signifie que le coin gauche de notre écran se trouve à la position horizontale 0. C'est comme si nous commençons à mesurer l'emplacement de notre écran à partir du côté gauche.

 - `yMinScreen = 0` : Cela signifie que le coin supérieur de notre écran se trouve à la position verticale 0. C'est comme si nous commençons à mesurer l'emplacement de notre écran depuis le haut.

 - `widthScreen = 240` : Cela définit la largeur de notre écran. Ici, la largeur est de 240 unités. Vous pouvez imaginer cela comme la distance d'un côté de l'écran à l'autre. Plus le chiffre est grand, plus l'écran est large.

 - `heightScreen = 240` : Cela définit la hauteur de notre écran. Ici, la hauteur est également de 240 unités. Vous pouvez imaginer cela comme la distance du haut de l'écran au bas. Plus le chiffre est grand, plus l'écran est haut.
``` py
xMinScreen = 0
yMinScreen = 0
widthScreen = 240
heightScreen = 240
```
La fonction `makeMoreOrLessLight()` a pour but de gérer la lumière de notre lampe torche, en fonction de la couleur que nous avons choisie, et de la dessiner sur l'écran.
``` py
def makeMoreOrLessLight():
    ##################################
    #    SOMETHING TO INSERT HERE    #
    #    DRAW THE COLOR ON SCREEN    #
    ##################################

    fillHSV(...)
    
    ###############################
    drawRectangle(xMinScreen, yMinScreen, widthScreen, heightScreen)
    delay(500)
```
Entre les commentaires `# SOMETHING TO INSERT HERE`, c'est l'endroit où vous pourriez insérer votre propre code pour contrôler la couleur de la lumière de la lampe. Vous pouvez choisir la couleur que vous préférez en utilisant la fonction `fillHSV(...)`. Par exemple, si vous voulez une lumière bleue, vous pourriez écrire `fillHSV(...)`, où `200` est la teinte du bleu.
``` py
##################################
#    SOMETHING TO INSERT HERE    #
#    DRAW THE COLOR ON SCREEN    #
##################################

fillHSV(...)
    
###############################
```
Ensuite, avec `drawRectangle(xMinScreen, yMinScreen, widthScreen, heightScreen)`, nous dessinons un rectangle qui remplit toute la surface de l'écran avec la couleur que nous avons choisie. Cela signifie que l'écran de la lampe torche sera illuminé de la couleur que vous avez définie.
```
drawRectangle(xMinScreen, yMinScreen, widthScreen, heightScreen)
```
`delay(500)` est utilisé pour faire en sorte que les changements de couleur ne se produisent pas trop rapidement. Il crée un petit délai de 500 millisecondes entre chaque fois qu'il dessine la couleur sur l'écran. 
``` py
delay(500)
```
La fonction `onDraw()` est une partie essentielle du code de la lampe torche. Elle gère ce qui se passe à l'écran en permanence. Voici comment cela fonctionne :
``` py
###############################
# Draw on the screen          #
###############################
def onDraw():
    makeMoreOrLessLight()
	update()
	if getButton():
		if returnToMenu():
			return
```
`makeMoreOrLessLight()` est la fonction que nous venons d'expliquer. Cela signifie que la fonction `makeMoreOrLessLight()` est exécutée régulièrement.
```
makeMoreOrLessLight()
```
`update()` met à jour l'écran pour montrer les changements que vous avez apportés. C'est comme appuyer sur un bouton "Actualiser" pour que les modifications que vous avez faites soient visibles à l'écran.
```
update()
```
## Le Final
Le résultat est une lampe qui s'allume en fonction de l'éclairage produit par son binôme ou même de la lumière présente en général <br>

![Lamp fonctionnel](page_FETG/ressources/Lamp.png){: style="height:300px;width:200px"; }