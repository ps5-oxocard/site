# Taux d'émission de Co2
<!------------------------------------------------------------------------------
- @brief       : co2 exercices
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : co2.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->

## Introduction
Dans ce code, nous allons explorer comment créer un graphique interactif pour afficher le taux de dioxyde de carbone (CO2), un élément crucial à surveiller dans notre environnement. Ces lignes de code ne sont pas simplement des caractères aléatoires, mais plutôt les instructions qui donneront vie à un cercle coloré capable de refléter le taux de CO2 en temps réel. Pour y parvenir, nous utilisons des constantes pour définir des paramètres clés, tels que les couleurs et les limites de CO2, que vous pourrez personnaliser selon vos préférences. Cependant, il y a une section que vous devrez remplir par vous-même pour obtenir les données de votre capteur de CO2. <br>
![Co2 fonctionnel_2](page_FETG/ressources/Co2_2.png){: style="height:250px;width:250px"; align=center}<br>
## Les Besoins
 - Un ordinateur
 - Une connexion à Internet
 - Une Oxocard Science
 - Un câble USB-USB type C
## Le code

Dans la première partie du code, nous avons défini des "constantes". Pensez-y comme des valeurs qui ne changent pas pendant l'exécution du programme, ce qui les rend constantes. <br>

Imaginez que vous configurez un jeu vidéo. Les constantes dans ce code sont un peu comme les paramètres que vous choisissez au début du jeu pour décider de la difficulté, des couleurs, ou d'autres aspects. Dans ce cas, nous avons choisi les constantes pour contrôler la couleur et les limites du taux de CO2 que nous voulons afficher. <br>

Pour les couleurs, nous avons décidé que `RED` (rouge) serait 195, `GREEN` (vert) serait 0, et `BLUE` (bleu) serait 201. Ces chiffres représentent l'intensité de chaque couleur. Vous pouvez changer les valeurs à droite si vous le désirez. <br>
``` py
const RED = 195         # 0 .. 255
const GREEN = 0         # 0 .. 255
const BLUE = 201        # 0 .. 255 
```
Ensuite, pour le taux de CO2, nous avons établi des bornes. La constante `PPM_Minimum` est à 390 et `PPM_Maximum` est à 530. Ces chiffres indiquent les valeurs minimales et maximales que nous mesurerons. <br>
``` py
const PPM_Minimum = 390 # 390 .. 400
const PPM_Maximum = 530 # 450 .. 800
```
L'idée principale est que ces constantes te permettent de personnaliser le comportement de ton programme en modifiant ces valeurs à un seul endroit. Cela signifie que si vous voulez changer la couleur ou les limites de mesure, vous n'avez qu'à le faire une fois dans le code, et cela affectera tout le reste. C'est une façon intelligente de garder votre code organisé et facile à adapter. <br>

Les variables qui suivent sont utilisées pour dessiner, elles servent aux différentes dimensions 
``` py
centerX = 120
centerY = 120
radius = 100
rateMax = 240
rate = 0
```
Les lignes 16 à 26 font partie de la fonction Co2RateCircle(), qui dessine le graphique du taux de CO2. <br>
la fonction `Co2RateCircle()` est comme une boîte magique qui dessine un cercle coloré pour représenter le taux de CO2. Chaque fois que vous voulez dessiner ce cercle, vous pouvez simplement "appeler" cette fonction. Cela rend le code plus organisé, plus facile à gérer, et vous permet de réutiliser des morceaux de code pour accomplir des tâches similaires sans répéter tout le processus. <br>
``` py
def Co2RateCircle():
    push()
    stroke(RED,GREEN,BLUE)
    noFill()
    stroke(RED,GREEN,BLUE)
    drawCircle(centerX, centerY, radius)
    drawRectangle(0,0,240,240)
    clipRectangle(0,rateMax-rate,240,240)
    fill(RED,GREEN,BLUE)
    drawCircle(centerX, centerY, radius)
    delay(1000)
    pop()
```
Tout d'abord, avec `push()`, nous sauvegardons l'état graphique actuel, un peu comme prendre une photo pour revenir en arrière plus tard.<br>
Ensuite, avec `stroke(RED, GREEN, BLUE)`, nous choisissons les couleurs pour le contour du cercle, en utilisant les constantes `RED` (rouge), `GREEN` (vert) et `BLUE` (bleu) que nous avons définies plus tôt. <br>
La commande `noFill()` indique que l'intérieur du cercle ne sera pas rempli de couleur, le laissant simplement avec un contour coloré. <br>
``` py
push()
stroke(RED,GREEN,BLUE)
noFill()
stroke(RED,GREEN,BLUE)
```
En utilisant les coordonnées du centre (`centerX et centerY`) et le rayon (`radius`), nous dessinons un cercle avec `drawCircle(centerX, centerY, radius)`. <br>
Un rectangle est également dessiné en arrière-plan avec `drawRectangle(0, 0, 240, 240)` pour servir de toile de fond. <br>
Ensuite, `clipRectangle(0, rateMax - rate, 240, 240)` crée une fenêtre de visualisation qui dépend de la variable rate pour montrer le taux de CO2. <br>
La commande `fill(RED, GREEN, BLUE)` spécifie que le cercle doit être rempli avec les mêmes couleurs que le contour. <br>
``` py
drawCircle(centerX, centerY, radius)
drawRectangle(0,0,240,240)
clipRectangle(0,rateMax-rate,240,240)
fill(RED,GREEN,BLUE)
```
Après avoir redessiné le cercle avec le remplissage de couleur défini, nous attendons une seconde avec `delay(1000)` pour que le graphique soit visible <br>
Puis avec `pop()`, nous revenons à l'état graphique précédent, comme si nous retirions la photo prise avec `push()`, préparant le graphique pour les prochaines étapes de dessin. <br>
``` py
drawCircle(centerX, centerY, radius)
delay(1000)
pop()
```
Ces lignes travaillent ensemble pour créer un graphique visuel du taux de CO2 avec des couleurs et une animation. <br>

La fonction `CO2()` est responsable de la gestion des données du taux de CO2 et de leur affichage. Il y a un espace réservé, marqué par des lignes de dièses ###, pour quelque chose que vous devez ajouter. Là où il est écrit `SOMETHING TO INSERT HERE`, c'est à vous d'insérer le code nécessaire pour obtenir le taux de CO2. <br>
``` py
def CO2():
    ##################################
    #    SOMETHING TO INSERT HERE
    #        TO GET THE CO2
    ##################################

    ppmValue = ...

    ##################################

    
    rate = (ppmValue - PPM_Minimum) * (rateMax / (PPM_Maximum - PPM_Minimum))
    ppm = strSubstring((ppmValue+""), 0, 6)
    delay(500)
    drawTextCentered(centerX, centerY, ppm)
```
Après avoir obtenu la valeur du taux de CO2, qui est stockée dans la variable `ppmValue`, les lignes suivantes effectuent quelques opérations. La variable rate est calculée en fonction de la différence entre `ppmValue` et la valeur minimale `PPM_Minimum`, ce qui est ensuite ajusté en utilisant la plage complète `rateMax` par rapport à la plage de PPM. Cela permet d'obtenir un taux ajusté qui peut être affiché sur le graphique.
``` py
rate = (ppmValue - PPM_Minimum) * (rateMax / (PPM_Maximum - PPM_Minimum))

```
Ensuite on utilise `strSubstring` pour convertir ppmValue en une chaîne de texte et la tronquer à 6 caractères. Cela permet d'obtenir une valeur du taux de CO2 propre et concise.
``` py
ppm = strSubstring((ppmValue+""), 0, 6)
```
Après, avec `delay(500)`, nous attendons une demi-seconde pour que le graphique soit stable avant d'afficher la valeur du taux de CO2. Enfin, avec `drawTextCentered(centerX, centerY, ppm)`, nous affichons le taux de CO2 au centre du cercle du graphique.
``` py
delay(500)
drawTextCentered(centerX, centerY, ppm)
```

Pour finir, la fonction `onDraw()` est appelé en boucle par l'appareil et redessine l'écran en continue avec les fonctions qu'on lui a dit de faire en boucle. 
``` py
def onDraw():
    background(0,0,0)
    Co2RateCircle()
    CO2()
	update()
    if getButton():
        if returnToMenu():
            return
```

## Le Final

Le résultat est un cercle qui se rempli en fonction du taux de Co2 présent dans la salle <br>
![Co2 fonctionnel](page_FETG/ressources/Co2.png){: style="height:300px;width:200px"; align=center}