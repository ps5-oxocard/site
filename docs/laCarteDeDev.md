# OXOCARD
<!------------------------------------------------------------------------------
- @brief       : Presentation of the cards
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : laCarteDeDev.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->
## Science

![Science](page_Cartes/ressources/science.png){: style="height:400px;width:400px"; align=center}<br>

L'Oxocard Science est une carte électronique sophistiquée dédiée à l'exploration scientifique. Mesurant la lumière, la température, le bruit, l'humidité, la pression, et plus encore, cette carte offre une expérience immersive avec des programmes préconçus et un code source documenté pour des expériences scientifiques personnalisées.

Caractéristiques Principales:

- Capteurs intégrés
- Écran couleur 240×240 RGB, WiFi, USB-C
- Expériences immédiates avec programmes préconçus

<br><br>
Le lien pour l'acheter : <https://shop.oxon.ch/?shop=oxocard-minis&lang=fr>
## Connect

L'Oxocard Connect est une carte électronique expérimentale compacte, équipée d'un microcontrôleur ESP32, de 2 MB de RAM, et de 8 MB de mémoire flash. Son design compact (48×36×7mm) et son boîtier de qualité avec couvercle en verre en font un outil pratique pour les passionnés d'électronique.

Caractéristiques Principales:

- Microcontrôleur ESP32
- 2 MB RAM, 8 MB Flash
- WiFi, Joystick 4 directions, Écran couleur 240×240 RGB
- Connexion USB-C, Connecteur Cartridge 16 broches

![Connect standard](page_Cartes/ressources/connect.png){: style="height:300px;width:500px"; align=center}<br>

Version Standard (Carte seule):

- Câble USB-C
- Guide de démarrage rapide

![Connect kit](page_Cartes/ressources/kitConnect.png){: style="height:500px;width:400px"; align=center}<br>

Version avec Kit des Innovateurs:

- 1 x Oxocard Connect
- 1 x Breadboard-Cartridge

Composants Électroniques dans le Kit:

- Capteur PIR (détecteur de mouvement)
- Thermistance 10kOhm (capteur de température)
- Photorésistance 10kOhm (capteur de lumière)
- Potentiomètre
- Micro-servo SG92R
- Piézo (signaux acoustiques)
- LED RVB
- 3 x LED (vert, jaune, rouge)
- 2 x Boutons
- 9 x Résistances
- 75 x Câbles (Angle) - Différentes couleurs et longueurs

Cette carte polyvalente offre une expérience d'apprentissage pratique de l'électronique, tandis que le kit des Innovateurs ajoute une dimension pratique avec des composants variés pour explorer une gamme étendue de projets.
<br><br>
Le lien pour l'acheter : <https://shop.oxon.ch/?shop=oxocard-connect&lang=fr>
