# Bienvenue
<!------------------------------------------------------------------------------
- @brief       : Welcome page of the exercices
- @author      : Kais Ouederni <kais.ouederni@edu.hefr.ch>
- @date        : 31. January 2024
- @file        : travauxPratiques.md
--------------------------------------------------------------------------------
- @copyright   : Copyright (c) 2023 HEIA-FR / ISC
-                Haute école d'ingénierie et d'architecture de Fribourg
-                Informatique et Systèmes de Communication
-------------------------------------------------------------------------------->

Nous sommes enchantés de vous accueillir dans notre atelier de programmation !

La programmation se révèle être un puissant outil vous permettant de concevoir des applications, des sites web, et bien plus encore, en explorant la manière dont les logiciels et les machines interagissent pour donner forme au monde numérique qui nous entoure.

Au cours de ces ateliers, que vous soyez néophyte ou que vous possédiez déjà un certain baguage expérience en programmation, vous découvrirez les bases de la programmation tout en développant des applications sur un petit système embarqué. L'objectif est de progresser tout en passant un moment agréable.

N'oubliez pas que la programmation stimule le développement de la créativité, de la logique, et de la pensée critique. C'est également une excellente préparation pour l'avenir, car les compétences en programmation sont de plus en plus recherchées dans divers domaines.
<br>
Quelques préparatifs...

Avant de démarrer, veuillez suivre ces étapes préliminaires.

1. Accédez à l'éditeur
        - [L'éditeur NanopPy](https://editor.nanopy.io/ )

2. Assurez-vous d'avoir une carte Oxocard Science et Connect à portée de mains car les ateliers ont besoin de soit la Oxocard Science, soit la Oxocard Connect

